# Books.com
This project allows a user to browse the books repository and create, delete and manage favourites lists.

# Structure
This project follows the clean code architecture. The folder `/src/app/features/` contains all business features divided into modules, where each module define its own services and components.
Each module structured as the following:
- Angular module definition file.
- Data folder: This folder includes the data repositories and data mappers (Mappers used to convert json object into dtos and vice-versa).
- Domain folder: This folder includes a folder for data classes and another folder for services.
- Presentation folder: This folder contains the required angular components.
 
- [Reactive Forms](https://angular.io/guide/reactive-forms) is used to handle form inputs and values validation.
- [PrimeNG](https://www.primefaces.org/primeng) is used as ui and styles framework.

# Modules
- **Auth Module**
This module contains the implementation for authentication and authorization functions.

- **Books Module**
This module contains the implementation for books management functions (Search books).

- **Lists Module**
This module contains the implementation for lists management functions (Search lists, Create new list, Edit list).

- **Shared Module**
This module contains some helpers and utils components and services.

# How to Use
- **Serve the application:**
    - Option 1:
        1. Clone this repository.
        2. Open the containing folder.
        3. Execute the following commands: `npm install` and `npm start`.
        4. You should be able to access the application from your browser on this url: `localhost:4200`.
    - Option 2:
        1. Download the dist from a the file **dist.zip**.
        2. Unzip the file into a folder.
        3. Serve the folder files using one of the static http server tools. e.g: [http-server](https://www.npmjs.com/package/http-server).
- **Use the application:**
Now your application is running and ready to use. the first page you will face is the login page. It's very simple page contains one text field and submit button; You need only yo enter any value as your name and submit the form; this will redirect you into books list page.

![Login Form](screenshots/01.png)
