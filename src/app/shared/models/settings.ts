/**
 * 
 */
export type LayoutMode = 'static' | 'overlay';

/**
 * 
 */
export type LayoutColor = 'light' | 'dark';

/**
 * 
 */
export type InputStyle = 'outlined' | 'filled';

/**
 * 
 */
export interface Settings {
    /**
     * 
     */
    layout: {
        /**
         * mode
         */
        mode: LayoutMode;

        /**
         * color
         */
        color: LayoutColor;
    };

    /**
     * 
     */
    inputStyle: InputStyle;

    /**
     * 
     */
    ripple: boolean;

    /**
     * 
     */
    menu: {
        static: boolean;
        overlay: boolean;
        mobile: boolean;
    };

    topbar: {
        state: boolean;
        mobile: boolean;
    };
}