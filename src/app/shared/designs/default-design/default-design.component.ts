import { Component } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent } from '@angular/router';
import { JObject } from '../../models/object';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-default-design',
  templateUrl: './default-design.component.html',
  styleUrls: ['./default-design.component.scss']
})
export class DefaultDesignComponent {

  public loading: boolean;
  public wrapperClass: JObject;

  constructor(private settings: ConfigService, private router: Router) {
    this.loading = false;
    this.wrapperClass = {};
 
    this.settings.settings.subscribe(next => {
      this.wrapperClass = {
        "layout-wrapper": true,
        "layout-overlay": next.layout.mode === "overlay",
        "layout-static": next.layout.mode === "static",
        "layout-landing-mode-active": !next.topbar.state,
        "layout-topbar-hidden": !next.topbar.state,
        "layout-static-sidebar-inactive": !next.menu.static && next.layout.mode === "static",
        "layout-overlay-sidebar-active": next.menu.overlay && next.layout.mode === "overlay",
        "layout-mobile-sidebar-active": next.menu.mobile,
        "p-input-filled": next.inputStyle === "filled",
        "p-ripple-disabled": !next.ripple,
        "layout-theme-light": next.layout.color === "light",
      };
    });

    this.router.events.subscribe((event) => {
      if (event instanceof RouterEvent) {
        this.checkRouterEvent(event);
      }
    });
  }

  private checkRouterEvent(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true;
    }

    if (event instanceof NavigationEnd || 
        event instanceof NavigationCancel || 
        event instanceof NavigationError) {
      this.loading = false;
    }
  }
}
