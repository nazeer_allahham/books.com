import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthRepository } from 'src/app/features/auth/data/repositories';
import { AuthService } from 'src/app/features/auth/domain';
import { FooterComponent } from '../../components/footer/footer.component';
import { SplashComponent } from '../../components/splash/splash.component';
import { ToastComponent } from '../../components/toast/toast.component';
import { TopbarComponent } from '../../components/topbar/topbar.component';

import { DefaultDesignComponent } from './default-design.component';

describe('DefaultDesignComponent', () => {
  let component: DefaultDesignComponent;
  let fixture: ComponentFixture<DefaultDesignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefaultDesignComponent, SplashComponent, TopbarComponent, FooterComponent, ToastComponent ],
      imports: [
        RouterTestingModule,
      ],
      providers: [
        AuthService,
        AuthRepository,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
