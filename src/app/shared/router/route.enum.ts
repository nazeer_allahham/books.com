/**
 * route url
 */
export enum RouteUrl {
    Login = '/auth/login',
    BookSearch = '/books',
    BookDetails = '/books/:bookId',
    ListSearch = '/lists',
    ListNew = '/lists/new',
    ListDetails = '/lists/:id',
}

export function isNeedAuth(url: string): boolean {
    return !!url && !!Object.values(RouteUrl).find(e => url === e + '')  && url !== (RouteUrl.Login +'');
}

export function isNeedGuest(url: string): boolean {
    return !!url && !!Object.values(RouteUrl).find(e => url === e + '')  && url === (RouteUrl.Login +'');
}