import { Component, HostBinding } from '@angular/core';
import { Image } from '../../models/image';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  public logo?: Image;

  @HostBinding("class.layout-footer")
  public className: boolean = true;

  constructor(private configService: ConfigService) {
    this.configService.logo.subscribe(next => this.logo = next);
  }
}
