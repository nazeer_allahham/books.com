import { Component } from '@angular/core';
import { AuthService } from 'src/app/features/auth/domain';
import { MenuItem } from '../../menu';
import { Image } from '../../models/image';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent {

  public logo?: Image;
  public items: MenuItem[] = [];

  constructor(private configService: ConfigService, private authService: AuthService) {

    this.configService.logo.subscribe(e => this.logo = e);
    this.authService.getProfile().subscribe(e => this.items = e?.menuItems ?? []);
  }
}
