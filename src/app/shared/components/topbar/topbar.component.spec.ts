import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthRepository } from 'src/app/features/auth/data/repositories';
import { AuthService } from 'src/app/features/auth/domain';
import { UserDropdownComponent } from 'src/app/features/auth/presentation/components/user-dropdown/user-dropdown.component';

import { TopbarComponent } from './topbar.component';

describe('TopbarComponent', () => {
  let component: TopbarComponent;
  let fixture: ComponentFixture<TopbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopbarComponent, UserDropdownComponent ],
      imports: [
        RouterTestingModule,
      ],
      providers: [
        AuthService,
        AuthRepository,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
