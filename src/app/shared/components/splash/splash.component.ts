import { Component } from '@angular/core';

import { Image } from '../../models/image';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent {

  public logo?: Image;

  constructor(private configService: ConfigService) {
    //
    this.configService.logo.subscribe(e => this.logo = e);
  }
}
