import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Image } from '../models/image';
import { Settings } from '../models/settings';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private _logo: BehaviorSubject<Image>;
  private _settings: BehaviorSubject<Settings>;


  constructor() {

    this._logo = new BehaviorSubject({ src: '/assets/img/books.png', alt: 'books.png' });
    this._settings = this._settings = new BehaviorSubject<Settings>({
      layout: {
        mode: 'static',
        color: 'light',
      },
      inputStyle: 'outlined',
      ripple: true,
      menu: {
        static: true,
        overlay: false,
        mobile: false,
      },
      topbar: {
        state: true,
        mobile: false,
      },
    });
  }

  public get logo() {
    return this._logo as Observable<Image>;
  }

  public get settings() {
    return this._settings as Observable<Settings>;
  }
}
