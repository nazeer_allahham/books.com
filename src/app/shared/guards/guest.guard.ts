import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';
import { map, Observable } from 'rxjs';
import { AuthService } from 'src/app/features/auth/domain';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanLoad {

  constructor(private authService: AuthService, private router: Router) {
  }

  public canLoad(route: Route, segments: UrlSegment[]): boolean {
    const r = this.authService.user === undefined;
    if (!r) {
      this.router.navigate(['/']);
    }
    return r;
  }
}
