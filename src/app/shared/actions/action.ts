import { Observable, of, throwError } from "rxjs";
import { PromptOptions } from "../prompt";
import { ActionCallback } from "./action.callback";
import { ActionName } from "./action.enum";

/**
 * 
 */
export class Action<T = void> {

    /**
     * name
     */
    public readonly name: ActionName;

    /**
     * title
     */
    public readonly title: string;

    /**
     * icon
     */
    public readonly icon: string;

    /**
     * callback
     */
    private readonly callback: ActionCallback<T>;

    /**
     * extra classname
     */
    public readonly className: string | undefined;

    /**
     * prompt
     */
    public readonly prompt: PromptOptions | undefined;

    constructor(name: ActionName, title: string, icon: string, callback: ActionCallback<T>);
    constructor(name: ActionName, title: string, icon: string, callback: ActionCallback<T>, className: string);
    constructor(name: ActionName, title: string, icon: string, callback: ActionCallback<T>, className: string | undefined, prompt: PromptOptions);
    constructor() {
        if (arguments.length < 4 || arguments.length > 6) {
            throw new Error("Invalid arguments length");
        }
        this.name = arguments[0];
        this.title = arguments[1];
        this.icon = arguments[2];
        this.callback = arguments[3];

        if (arguments.length >= 5) {
            this.className = arguments[4];

            if (arguments.length >= 6) {
                this.prompt = arguments[5];
            }
        }
    }

    /**
     * arguments
     */
    public run(...args: Array<any>): Observable<T> {
        try {
            const obs = this.callback(...args);
            if (obs instanceof Observable) {
                return obs;
            }
            return of(obs);
        } catch (error) {
            return throwError(() => error);
        }
    }
}