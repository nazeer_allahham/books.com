import { Observable } from "rxjs";

/**
 * action callback
 */
export type ActionCallback<T> = (...args: Array<any>) => T | Observable<T>;
