import { MenuItem as PNMenuItem, PrimeIcons } from 'primeng/api';
import { RouteUrl } from '../router';

/**
 * 
 */
export type MenuItem = PNMenuItem;

/**
 * 
 */
export const allMenuItems: Array<MenuItem> = [
    {
        label: 'Books',
        icon: PrimeIcons.BOOKMARK,
        url: RouteUrl.BookSearch,
    },
    {
        label: 'Lists',
        icon: PrimeIcons.LIST,
        url: RouteUrl.ListSearch,
    },
];