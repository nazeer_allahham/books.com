import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ProgressBarModule } from 'primeng/progressbar';
import { ToastModule } from 'primeng/toast';

import { DefaultDesignComponent } from './designs';
import { SplashComponent } from './components/splash/splash.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthModule } from '../features/auth/auth.module';
import { MessageService } from 'primeng/api';
import { ToastComponent } from './components/toast/toast.component';


@NgModule({
  declarations: [
    DefaultDesignComponent,
    SplashComponent,
    TopbarComponent,
    FooterComponent,
    ToastComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    ProgressBarModule,
    AuthModule,
    ToastModule,
  ],
  exports: [
    DefaultDesignComponent,
  ],
  providers: [
    MessageService,
  ]
})
export class SharedModule {  
}
