/**
 * prompt options
 */
 export type PromptOptions = boolean | ({mode: 'normal'});