import { ActionName } from "src/app/shared/actions";
import { allMenuItems, MenuItem } from "src/app/shared/menu";
import { RouteUrl } from "src/app/shared/router";
import { UserPermissions } from "./permission";
import { Prefrences } from "./prefrences";
import { User } from "./user";

/**
 * AppUser
 */
export class AppUser {

    /**
     * user
     */
    public readonly user: User;

    /**
     * prefrences
     */
    public readonly prefrences: Prefrences;

    /**
     * route url
     */
    public readonly routes: RouteUrl[];

    /**
     * menu items
     */
    public readonly menuItems: MenuItem[];

    /**
     * permissions
     */
    private readonly permissions: UserPermissions;


    constructor(user: User, prefrences: Prefrences, permissions: UserPermissions) {
        //
        this.user = user;
        this.prefrences = prefrences;
        //
        this.permissions = permissions;

        const check = (route: RouteUrl, permissions: UserPermissions) => {
            return route && permissions && permissions[route] && permissions[route].includes('R');
        }
        this.routes = Object.values(RouteUrl).filter(e => check(e, this.permissions));

        const checkMenu = (menu: Array<MenuItem>): Array<MenuItem> => {
            const result = new Array<MenuItem>();
            menu.forEach(e => {
                if (e.url && this.canLoad(e.url as RouteUrl)) {
                    result.push(e);
                } else if (e.items) {
                    const items = checkMenu(e.items);
                    if (items && items.length) {
                        result.push({ ...e, items: items });
                    }
                }
            });
            return result;
        }
        this.menuItems = checkMenu(allMenuItems);
    }


    /**
     * Check if this user can load route 
     * @param route route
     * @returns {boolean}
     */
    public canLoad(route: RouteUrl): boolean {
        return this.routes && this.routes.includes(route);
    }

    /**
     * Check if this user can load route 
     * @param route route
     * @returns {boolean}
     */
    public canExecute(route: RouteUrl, actionName: ActionName): boolean {
        return this.canLoad(route) && this.permissions[route] && this.permissions[route].includes(actionName);
    }
}