/**
 * Prefrences
 */
export interface Prefrences {

    /**
     * rememberMe
     */
    rememberMe: boolean;
}