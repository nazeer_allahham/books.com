import { ActionName } from "src/app/shared/actions";

/**
 * role name
 */
export enum Role {
    /**
     * User
     */
    User = 'user',
}

/**
 * route operation:
 * an operation could be 'R' which means the access to read OR a ActionName
 */
export type RouteOperation = 'R' | ActionName;

/**
 * User permissions
 */
export type UserPermissions = {
    /**
     * 
     */
    [key: string]: Array<RouteOperation>;
};
