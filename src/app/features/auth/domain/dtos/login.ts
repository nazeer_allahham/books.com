/**
 * Login
 */
export interface LoginDto {
    /**
     * 
     */
    username: string;

    /**
     * 
     */
    keep: boolean;
}