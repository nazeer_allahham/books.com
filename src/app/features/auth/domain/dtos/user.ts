/**
 * User
 */
export interface User {

    /**
     * username
     */
    username: string;
}
