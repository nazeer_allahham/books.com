import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of, tap } from "rxjs";
import { ActionName } from "src/app/shared/actions";
import { RouteUrl } from "src/app/shared/router";

import { AuthRepository } from "../../data/repositories";
import { AppUser, LoginDto } from "../dtos";

@Injectable()
export class AuthService {

    private readonly _repo: AuthRepository;
    private readonly _user: BehaviorSubject<AppUser | undefined>;

    constructor(repository: AuthRepository) {

        this._repo = repository;
        this._user = new BehaviorSubject<AppUser | undefined>(undefined);
    }

    public get user() {
        return this._user.value;
    }

    /**
     * Get authenticated user profile
     */
    public getProfile(force: boolean = false): Observable<AppUser> {
        if (!force && this._user.value) {
            return of(this._user.value);
        }
        return this._repo.getProfile().pipe(tap(res => this._user.next(res)));
    }


    /**
     * Login
     * @param dto
     */
    public login(dto: LoginDto): Observable<boolean> {
        return this._repo.login(dto);
    };

    /**
     * Logout
     */
    public logout(): Observable<void> {
        return this._repo.logout().pipe(tap(() => this._user.next(undefined)));
    };

    /**
     * check if the authenticated appUser authorized to access the given action over the given route
     * @param route route
     * @param actionName actionName
     * @returns {boolean}
     */
    public authorized(route: RouteUrl | undefined, actionName: ActionName): boolean {
        const u = this._user.value;
        if (route && u) {
            // if route doesn't need auth then return true
            if (u.canExecute(route, actionName)) {
                return true;
            }
        }
        return false;
    }
}
