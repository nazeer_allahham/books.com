import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuModule } from 'primeng/menu';
import { MessageModule } from 'primeng/message';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { CheckboxModule } from 'primeng/checkbox';
import { CardModule } from 'primeng/card';

import { UserDropdownComponent } from './presentation/components/user-dropdown/user-dropdown.component';
import { LoginComponent } from './presentation/components/login/login.component';
import { AuthService } from './domain/services';
import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRepository } from './data/repositories';



@NgModule({
  declarations: [
    UserDropdownComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    MenuModule,
    MessageModule,
    ButtonModule,
    AuthRoutingModule,
    InputTextModule,
    PasswordModule,
    CheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule
  ],
  exports: [
    UserDropdownComponent,
    // AuthService,
    // AuthResolver,
  ],
  providers: [
    AuthService,
    AuthRepository,
  ]
})
export class AuthModule {
}
