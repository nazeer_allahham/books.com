import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Image } from 'src/app/shared/models/image';
import { ConfigService } from 'src/app/shared/services/config.service';
import { AuthService } from '../../../domain';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public logo?: Image;
  public loading: boolean = false;
  public form: FormGroup;

  constructor(private configService: ConfigService, private authService: AuthService, private router: Router) {

    this.configService.logo.subscribe(e => this.logo = e);
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      keep: new FormControl(true),
    });
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    const value = this.form.value;

    this.authService
      .login({ username: value.username, keep: value.keep })
      .subscribe(res => {
        if (res) {
          window.location.href = '/';
        }
      })
  }
}
