import { Component } from '@angular/core';
import { MenuItem, PrimeIcons } from 'primeng/api';
import { AuthService, User } from '../../../domain';

@Component({
  selector: 'app-user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.scss']
})
export class UserDropdownComponent {

  public user?: User;
  public items: MenuItem[];

  constructor(private authService: AuthService) {

    this.items = [];
    this.authService.getProfile().subscribe(next => {

      if (next) {
        this.user = next.user;
        this.items = [
          {
            label: next.user.username,
          },
          {
            separator: true
          },
          {
            icon: PrimeIcons.POWER_OFF,
            label: 'Logout',
            command: () => this.logout(),
          }
        ];
      } else {
        this.user = undefined;
        this.items = [];
      }
    });
  }

  private logout(): void {
    this.authService.logout().subscribe();
    window.location.href = '/';
  }
}
