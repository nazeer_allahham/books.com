import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuModule } from 'primeng/menu';
import { AuthRepository } from '../../../data/repositories';
import { AuthService } from '../../../domain';

import { UserDropdownComponent } from './user-dropdown.component';

describe('UserDropdownComponent', () => {
  let component: UserDropdownComponent;
  let fixture: ComponentFixture<UserDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDropdownComponent ],
      imports: [
        MenuModule,
      ],
      providers: [
        AuthService,
        AuthRepository,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
