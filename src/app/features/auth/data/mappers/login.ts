import { JObject } from "src/app/shared/models/object";
import { LoginDto } from "../../domain";

export const LoginDtoMapper = {

    /**
     *
     */
    toJson: (dto: LoginDto): JObject => {
        if (!dto.username || !dto.username.length) {
            throw new Error("dto.username");
        }
        return {
            username: dto.username,
        };
    },
}
