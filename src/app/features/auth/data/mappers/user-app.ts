import { JObject } from "src/app/shared/models/object";
import { AppUser, UserPermissions } from "../../domain";
import { PrefrencesMapper } from "./prefrences";
import { UserMapper } from "./user";

export const AppUserMapper = {

    /**
     * fromJson
     * 
     * @param json
     * @returns {AppUser}
     */
    fromJson: (json: JObject, permissions: UserPermissions): AppUser => {
        return new AppUser(UserMapper.fromJson(json['user']), PrefrencesMapper.fromJson(json['prefrences']), permissions);
    },

    /**
     * toJson
     * 
     * @param appUser
     * @returns {JObject}
     */
    toJson: (appUser: AppUser): JObject => {
        return { user: UserMapper.toJson(appUser.user), prefrences: PrefrencesMapper.toJson(appUser.prefrences) };
    },
}