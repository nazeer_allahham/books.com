import { JObject } from "src/app/shared/models/object";
import { User } from "../../domain";

export const UserMapper = {

    /**
     * fromJson
     * @param json
     * @returns {User}
     */
    fromJson: (json: JObject): User => {
        return {
            username: json['username'],
        }
    },

    /**
     *
     */
    toJson: (user: User): JObject => {
        return { username: user.username }
    },
}
