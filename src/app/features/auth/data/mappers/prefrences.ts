import { JObject } from "src/app/shared/models/object";
import { Prefrences } from "../../domain";

export const PrefrencesMapper = {

    /**
     * fromJson
     * @param json
     * @returns {Prefrences}
     */
    fromJson: (json: JObject): Prefrences => {
        return {
            rememberMe: json['rememberMe'],
        }
    },

    /**
     *
     */
    toJson: (prefrences: Prefrences): JObject => {
        return { rememberMe: prefrences.rememberMe }
    },
}
