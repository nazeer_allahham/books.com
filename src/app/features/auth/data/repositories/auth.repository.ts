import { Injectable } from "@angular/core";
import { delay, map, Observable, of, throwError } from "rxjs";

import { JObject } from "src/app/shared/models/object";
import { LoginDto, Prefrences, Role, User } from "../../domain";
import { AppUser } from "../../domain/dtos/user-app";
import { LoginDtoMapper, UserMapper } from "../mappers";
import { PermissionsRepository } from "./permissions.repository";

@Injectable()
export class AuthRepository {

    private static USER_KEY = '_user';
    private static PREFRENCES_KEY = '_prefrences';

    private readonly permissions = new PermissionsRepository();
    private prefrences?: Prefrences;
    private storage?: Storage;

    constructor() {
        this.getPrefrences();
    }

    private setPrefrences(rememberMe: boolean): void {
        const prefrenecs = {
            rememberMe,
        };
        this.prefrences = prefrenecs;
        this.storage = rememberMe ? localStorage : sessionStorage;
        localStorage.setItem(AuthRepository.PREFRENCES_KEY, JSON.stringify(prefrenecs));
    }

    private getPrefrences(): void {
        const key = localStorage.getItem(AuthRepository.PREFRENCES_KEY) || '{}';
        const prefrences = JSON.parse(key);
        this.storage = prefrences.rememberMe ? localStorage : sessionStorage;
        this.prefrences = prefrences;
    }

    private setUser(obj: JObject): void {
        this.storage!.setItem(AuthRepository.USER_KEY, JSON.stringify(obj));
    }

    private getUser(): User | undefined {
        try {
            const key = this.storage!.getItem(AuthRepository.USER_KEY);
            if (!key) {
                return undefined;
            }
            const user = JSON.parse(key);
            return UserMapper.fromJson(user);
        } catch (e) {
            return undefined;
        }
    }

    /**
     * Login
     * @param dto
     */
    public login(dto: LoginDto): Observable<boolean> {
        try {
            const data = LoginDtoMapper.toJson(dto);

            this.setPrefrences(dto.keep);
            this.setUser(data);

            return of(true);

        } catch (e) {
            return throwError(() => e);
        }
    };

    /**
     * Login
     * @param dto
     */
    public logout(): Observable<void> {
        try {
            if (!this.storage) {
                throw new Error('Storage not initialized');
            }
            this.storage.clear();
            return of();

        } catch (e) {
            return throwError(() => e);
        }
    };

    /**
     * Get authenticated user profile
     */
    public getProfile(): Observable<AppUser> {
        const u = this.getUser();
        if (!u || !this.prefrences) {
            return throwError(() => new Error('Failed to detect user or prefrences object.'));
        }

        return this.permissions
            .getPermissions(Role.User)
            .pipe(map(res => new AppUser(u, this.prefrences!, res)));
    }
}
