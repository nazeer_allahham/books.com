import { Observable, of, throwError } from "rxjs";
import { RouteUrl } from "src/app/shared/router";
import { Role, UserPermissions } from "../../domain";

export class PermissionsRepository {

    private readonly db: { [key: string]: UserPermissions } = {
        'user': {
            [RouteUrl.BookSearch]: ['R'],
            [RouteUrl.BookDetails]: ['R'],
            [RouteUrl.ListSearch]: ['R'],
            [RouteUrl.ListNew]: ['R'],
            [RouteUrl.ListDetails]: ['R'],
        }
    };

    /**
     * permissions
     */
    public getPermissions(role: Role): Observable<UserPermissions> {
        if ([Role.User].includes(role)) {
            return of(this.db[role]);
        }
        return throwError(() => new Error('Failed to detect the role from the passed name'));
    }
}
