import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListsRoutingModule } from './lists-routing.module';
import { ListsListComponent } from './presentation/components/lists-list/lists-list.component';
import { ListsListItemComponent } from './presentation/components/lists-list-item/lists-list-item.component';
import { ListFormComponent } from './presentation/components/list-form/list-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataViewModule } from 'primeng/dataview';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { ListBooksFormComponent } from './presentation/components/list-books-form/list-books-form.component';
import { InputNumberModule } from 'primeng/inputnumber';
import { BooksModule } from '../books/books.module';
import { DropdownModule } from 'primeng/dropdown';
import { TagModule } from 'primeng/tag';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { ProgressSpinnerModule } from 'primeng/progressspinner';


@NgModule({
  declarations: [
    ListsListComponent,
    ListsListItemComponent,
    ListFormComponent,
    ListBooksFormComponent
  ],
  imports: [
    CommonModule,
    ListsRoutingModule,
    FormsModule,
    DataViewModule,
    ButtonModule,
    InputTextModule,
    MessageModule,
    ReactiveFormsModule,
    DialogModule,
    InputNumberModule,
    BooksModule,
    DropdownModule,
    TagModule,
    ConfirmDialogModule,
    ProgressSpinnerModule,
  ],
  providers: [
    ConfirmationService
  ],
  exports: [
  ]
})
export class ListsModule { }
