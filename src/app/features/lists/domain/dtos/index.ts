/**
 * List book
 */
export interface ListBook {
    /**
     * 
     */
    bookId: string;

    /**
     * 
     */
    order: number;
}

export interface List {

    listId: string;

    /**
     * 
     */
    title: string;

    /**
     * books
     */
    books: ListBook[];
}