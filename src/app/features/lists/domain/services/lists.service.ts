import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JObject } from 'src/app/shared/models/object';
import { ListsRepository } from '../../data/repositories/lists.repository';
import { List } from '../dtos';

@Injectable({
  providedIn: 'root'
})
export class ListsService {

  constructor(private repo: ListsRepository) {
  }

  /**
 * Search the available lists list.
 * @param q search query
 * @returns {Observable<List[]>}
 */
  public search(): Observable<List[]> {
    return this.repo.search();
  }

  public get(title: string): Observable<List> {
    return this.repo.get(title);
  }

  /**
   * Create list
   * 
   * @returns {Observable<List>}
   */
  public createOrSave(obj: JObject): Observable<List> {
    return this.repo.createOrSave(obj);
  }

  /**
   * Delete list by title
   * @returns {Observable<boolean>}
   */
  public delete(title: string): Observable<boolean> {
    return this.repo.delete(title);
  }

}
