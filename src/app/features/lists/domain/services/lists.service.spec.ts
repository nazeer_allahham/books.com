import { TestBed } from '@angular/core/testing';
import { AuthRepository } from 'src/app/features/auth/data/repositories';
import { AuthService } from 'src/app/features/auth/domain';
import { ListsRepository } from '../../data/repositories/lists.repository';

import { ListsService } from './lists.service';

describe('ListsService', () => {
  let service: ListsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ListsRepository,
        AuthService,
        AuthRepository,
      ]
    });
    service = TestBed.inject(ListsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
