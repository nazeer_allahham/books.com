import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of, throwError } from "rxjs";
import { delay, filter } from "rxjs/operators";
import { AuthService } from "src/app/features/auth/domain";
import { BooksService } from "src/app/features/books/domain/services/books.service";
import { JObject } from "src/app/shared/models/object";
import { ObjectUtils } from "src/app/utils/object";
import { List } from "../../domain/dtos";
import { ListMapper } from "../mappers/list";

@Injectable({
    providedIn: 'root'
})
export class ListsRepository {

    /**
     * 
     */
    private static LISTS_KEY = '_lists';

    /**
     * storage
     */
    private storage?: Storage;
    private value: BehaviorSubject<List[]>;

    constructor(private authService: AuthService) {
        this.value = new BehaviorSubject<List[]>([]);
        this.authService.getProfile()
            .subscribe(e => {
                this.storage = undefined;
                if (e && e.prefrences) {
                    if (e.prefrences.rememberMe) {
                        this.storage = localStorage;
                    } else {
                        this.storage = sessionStorage;
                    }
                    this.restore();
                }
            }, err => {
                
            });
    }

    private store(value: List[]): void {
        if (!this.storage) {
            throw new Error("msgs_storage_not_initialized");
        }
        let e: any = value;
        e = e.map((i: any) => ListMapper.toJson(i));
        e = JSON.stringify(e);
        this.storage.setItem(ListsRepository.LISTS_KEY, e);
        this.value.next(value);
    }

    private restore(): void {
        if (!this.storage) {
            throw new Error("msgs_storage_not_initialized");
        }
        let value: any = this.storage.getItem(ListsRepository.LISTS_KEY);
        value = value || '[]';
        value = JSON.parse(value);
        if (!Array.isArray(value)) {
            throw new Error("msgs_error_unexpected");
        }
        value = value.map((e: any) => ListMapper.fromJson(e));
        this.value.next(value);
    }

    /**
     * Search the available lists list.
     * @param q search query
     * @returns {Observable<List[]>}
     */
    public search(): Observable<List[]> {
        return this.value as Observable<List[]>;
    }

    /**
     * Create list
     * 
     * @returns {Observable<List>}
     */
    public createOrSave(obj: JObject): Observable<List> {
        try {
            obj['listId'] = obj['listId'] || ObjectUtils.random();

            const list = ListMapper.fromJson(obj);
            const value = this.value.value;
            const index = value.findIndex(e => e.listId === list.listId);
            if (index < 0 || index >= value.length) {
                value.push(list);
            } else {
                value[index] = list;
            }
            this.store(value);
            return of(list).pipe(delay(1500));
        } catch (error) {
            return throwError(() => error);
        }
    }

    /**
     * Delete
     * @returns {Observable<boolean>}
     */
    public delete(id: string): Observable<boolean> {
        try {
            const value = this.value.value;
            const list = value.find(i => i.listId === id);
            if (!list) {
                throw new Error('Invalid list identifier.');
            }
            value.splice(value.indexOf(list), 1);
            this.store(value);
            return of(true).pipe(delay(1500));
        } catch (error) {
            return throwError(() => error);
        }
    }

    /**
     * 
     * @param id 
     * @returns {Observable<List>}
     */
    public get(id: string): Observable<List> {
        try {
            const value = this.value.value;
            const list = value.find(i => i.listId === id);
            if (!list) {
                throw new Error('Invalid list identifier');
            }
            return of(list).pipe(delay(1500));
        } catch (error) {
            return throwError(() => error);
        }
    }
}