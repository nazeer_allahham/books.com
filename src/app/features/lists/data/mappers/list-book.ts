import { JObject } from "src/app/shared/models/object";
import { ListBook } from "../../domain/dtos";

export const ListBookMapper = {

    /**
     * fromJson
     * @param json
     * @returns {ListBook}
     */
    fromJson: (json: JObject): ListBook => {
        return {
            bookId: json['bookId'],
            order: json['order'],
        }
    },

    /**
     *
     */
    toJson: (book: ListBook): JObject => {
        return { order: book.order, bookId: book.bookId };
    },
}
