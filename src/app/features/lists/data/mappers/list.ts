import { BookMapper } from "src/app/features/books/data/mappers/book";
import { JObject } from "src/app/shared/models/object";
import { List, ListBook } from "../../domain/dtos";
import { ListBookMapper } from "./list-book";

export const ListMapper = {

    /**
     * fromJson
     * @param json
     * @returns {List}
     */
    fromJson: (json: JObject): List => {
        return {
            listId: json['listId'],
            title: json['title'],
            books: (json['books'] && Array.isArray(json['books'])) ? json['books'].map(i => ListBookMapper.fromJson(i)) : [],
        }
    },

    /**
     *
     */
    toJson: (list: List): JObject => {
        return {
            listId: list.listId,
            title: list.title,
            books: list.books.map(i => ListBookMapper.toJson(i)),
        };
    },
}
