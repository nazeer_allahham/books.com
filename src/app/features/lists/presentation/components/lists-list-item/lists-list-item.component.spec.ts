import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfirmationService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { AuthRepository } from 'src/app/features/auth/data/repositories';
import { AuthService } from 'src/app/features/auth/domain';
import { ListsRepository } from '../../../data/repositories/lists.repository';
import { ListsService } from '../../../domain/services/lists.service';

import { ListsListItemComponent } from './lists-list-item.component';

describe('ListsListItemComponent', () => {
  let component: ListsListItemComponent;
  let fixture: ComponentFixture<ListsListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListsListItemComponent ],
      imports: [
        RouterTestingModule,
        ButtonModule,
        ConfirmDialogModule,
      ],
      providers: [
        ConfirmationService,
        ListsService,
        ListsRepository,
        AuthService,
        AuthRepository,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
