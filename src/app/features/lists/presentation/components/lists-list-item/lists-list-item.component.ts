import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, ConfirmEventType } from 'primeng/api';
import { List } from '../../../domain/dtos';
import { ListsService } from '../../../domain/services/lists.service';

@Component({
  selector: 'app-lists-list-item',
  templateUrl: './lists-list-item.component.html',
  styleUrls: ['./lists-list-item.component.scss']
})
export class ListsListItemComponent {

  @Input()
  public list?: List;

  constructor(private router: Router, private confirmationService: ConfirmationService, private lists: ListsService) {
  }

  public onEdit(): void {
    if (this.list) {
      this.router.navigate(['/lists/' + this.list.listId])
    }
  }

  public onDelete(): void {
    this.confirmationService.confirm({
      message: 'Do you want to delete this list?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        if (this.list) {
          this.lists.delete(this.list.listId).subscribe();
          this.confirmationService.close();
        }
      },
      reject: (type: ConfirmEventType) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            break;
          case ConfirmEventType.CANCEL:
            break;
        }
        this.confirmationService.close();
      }
    });
  }
}
