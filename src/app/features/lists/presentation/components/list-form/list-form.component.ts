import { Component, OnDestroy } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { catchError } from 'rxjs';
import { Book } from 'src/app/features/books/domain/dtos';
import { ListsService } from '../../../domain/services/lists.service';

@Component({
  selector: 'app-list-form',
  templateUrl: './list-form.component.html',
  styleUrls: ['./list-form.component.scss']
})
export class ListFormComponent implements OnDestroy {

  // states
  public loading: boolean;
  public saving: boolean;
  public error: Error | undefined;

  // dialog props
  public header: string;
  public visible: boolean;

  // list props
  public form: FormGroup;

  // helper props
  public books: (Book & { selected?: boolean })[];

  /**
   * 
   * @param router router
   * @param lists lists
   * @param route route
   */
  constructor(private router: Router, private lists: ListsService, private route: ActivatedRoute, private toaster: MessageService) {

    // states
    this.loading = false;
    this.saving = false;
    this.error = undefined;

    // dialog props
    this.header = 'Create New List';  // header value will be changed in case of params subscription pushed new value
    this.visible = true;

    // list props
    this.form = new FormGroup({
      listId: new FormControl(undefined),
      title: new FormControl(undefined, [Validators.required]),
      books: new FormArray([]),
    });

    // helper props
    this.books = [];

    // listening to route params changes
    this.route.params.subscribe(params => {
      const { id } = params;
      if (id) {
        this.loading = true;
        this.lists
          .get(id)
          .pipe(catchError((err, caught) => {
            this.loading = false;
            this.toaster.add({ severity: 'error', detail: err.message });
            return caught;
          }))
          .subscribe(res => {
            const list = res;
            if (list) {
              // here we will update the form values in two steps
              // 1. patch values of listId and title
              this.form.patchValue({ listId: list.listId, title: list.title });
              // 2. update books
              const books = this.form.controls['books'] as FormArray;
              // 2.1 clear controls is recommened here to make sure no controls added.
              books.clear();
              // 2.2 iterate list's books
              list.books.forEach(e => books.push(new FormGroup({
                bookId: new FormControl(e.bookId, [Validators.required]),
                order: new FormControl(e.order, [Validators.required])
              })));
            }
            this.loading = false;
          });
      }
    });
  }

  public ngOnDestroy(): void {
    this.visible = false;
  }

  public get statusMessage(): string {
    if (this.loading) {
      return 'Loading...';
    } else if (this.saving) {
      return 'Saving...';
    } else if (this.form.value.id) {
      return `Editing List '${this.form.value}'...`;
    } else {
      return `Creating New List...`;
    }
  }

  public get submitText(): string {
    if (this.loading) {
      return 'Loading...';
    } else if (this.saving) {
      return 'Saving...';
    }
    return 'Save';
  }

  public onHide(): void {
    this.router.navigate(['/lists']);
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    const value = this.form.value;
    this.saving = true;
    this.lists.createOrSave(value)
      .pipe(catchError((err, caught) => {
        this.saving = false;
        this.error = err as Error;
        return caught;
      }))
      .subscribe(() => {
        this.saving = false;
        this.onHide();
      });
  }
}
