import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { MessageModule } from 'primeng/message';
import { AuthRepository } from 'src/app/features/auth/data/repositories';
import { AuthService } from 'src/app/features/auth/domain';
import { ListsRepository } from '../../../data/repositories/lists.repository';
import { ListsService } from '../../../domain/services/lists.service';
import { ListBooksFormComponent } from '../list-books-form/list-books-form.component';

import { ListFormComponent } from './list-form.component';

describe('ListFormComponent', () => {
  let component: ListFormComponent;
  let fixture: ComponentFixture<ListFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListFormComponent, ListBooksFormComponent ],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        ButtonModule,
        MessageModule,
        DialogModule,
        ReactiveFormsModule,
      ],
      providers: [
        ListsService,
        ListsRepository,
        AuthService,
        AuthRepository,
        MessageService,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
