import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonModule } from 'primeng/button';
import { DataViewModule } from 'primeng/dataview';
import { AuthRepository } from 'src/app/features/auth/data/repositories';
import { AuthService } from 'src/app/features/auth/domain';
import { ListsRepository } from '../../../data/repositories/lists.repository';
import { ListsService } from '../../../domain/services/lists.service';
import { ListsListItemComponent } from '../lists-list-item/lists-list-item.component';

import { ListsListComponent } from './lists-list.component';

describe('ListsListComponent', () => {
  let component: ListsListComponent;
  let fixture: ComponentFixture<ListsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListsListComponent, ListsListItemComponent ],
      imports: [
        RouterTestingModule,
        DataViewModule,
        ButtonModule,
      ],
      providers: [ListsService, ListsRepository, AuthService, AuthRepository],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
