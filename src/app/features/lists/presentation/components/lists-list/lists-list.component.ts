import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataView } from 'primeng/dataview';
import { List } from '../../../domain/dtos';
import { ListsService } from '../../../domain/services/lists.service';

@Component({
  selector: 'app-lists-list',
  templateUrl: './lists-list.component.html',
  styleUrls: ['./lists-list.component.scss']
})
export class ListsListComponent implements OnInit {

  public loading: boolean;
  public lists: List[];
  public error?: Error;

  constructor(private listsService: ListsService, private route: ActivatedRoute) {

    this.loading = true;
    this.lists = [];
  }

  public ngOnInit(): void {
    this.onSearch();
  }

  /**
   * On search
   * @param q search query
   */
  public onSearch(): void {
    this.loading = true;
    this.listsService.search().subscribe(res => {

      console.log('lists', res);

      this.lists = res;
      this.loading = false;
    });
  }

  public get data() {
    return Array.from(this.lists ?? []);
  }
}
