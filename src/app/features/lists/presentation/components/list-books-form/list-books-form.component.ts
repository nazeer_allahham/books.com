import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { catchError } from 'rxjs';
import { Book } from 'src/app/features/books/domain/dtos';
import { BooksService } from 'src/app/features/books/domain/services/books.service';

@Component({
  selector: 'app-list-books-form',
  templateUrl: './list-books-form.component.html',
  styleUrls: ['./list-books-form.component.scss'],
})
export class ListBooksFormComponent implements OnInit {

  public loading: boolean;
  public books: (Book & { selected?: boolean })[];
  public error?: Error;

  @Input()
  public control?: AbstractControl;

  constructor(private booksService: BooksService) {

    this.loading = true;
    this.books = [];
  }

  public ngOnInit(): void {
    this.onSearch();

    if (this.formArray) {
      this.formArray.valueChanges.subscribe(e => {
        const titles = e.map((i: any) => i.bookId);
        this.books.forEach(e => e.selected = titles.includes(e.title));
      });
    }
  }

  public get formArray() {
    return this.control as FormArray;
  }

  /**
   * On search
   * @param q search query
   */
  public onSearch(): void {
    this.loading = true;
    this.booksService
        .search()
        .pipe(catchError((err, caught) => {

          this.error = err as Error;
          this.loading = false;
          return caught;
        }))
        .subscribe(res => {

          this.books = res;
          this.loading = false;
        });
  }

  public canNew(): boolean {
    return !this.formArray || this.formArray.valid;
  }

  public addNew(): void {
    const books = this.formArray;
    books.push(new FormGroup({
      bookId: new FormControl(undefined, [Validators.required]),
      order: new FormControl(books.length, [Validators.required]),
    }));
  }

  public assertFormGroup(c: AbstractControl): FormGroup {
    if (!c || !(c instanceof FormGroup)) {
      throw new Error();
    }
    return c;
  }

  public delete(i: number): void {
    const books = this.formArray;
    if (i >= 0 && i < books.controls.length) {
      books.removeAt(i);
    }
  }

  public moveUp(i: number): void {
    const books = this.formArray;
    const book = books.at(i);
    if (book) {
      const order = book.value.order;
      if (order > 0) {
        const i2 = books.controls.findIndex(e => e.value.order === order - 1);
        const book2 = books.at(i2);
        if (book2) {

          book.patchValue({ order: order - 1 });
          book2.patchValue({ order: order });
        }
      }
    }
  }

  public moveDown(i: number): void {
    const books = this.formArray;
    const book = books ? books.at(i) : undefined;
    if (book) {
      const order = book.value.order;
      if (order < books.length - 1) {
        const i2 = books.controls.findIndex(e => e.value.order === order + 1);
        const book2 = books.at(i2);
        if (book2) {
          book.patchValue({ order: order + 1 });
          book2.patchValue({ order: order });
        }
      }
    }
  }

  public ordered(controls: AbstractControl[]): AbstractControl[] {
    return controls.sort((a, b) => {
      const fga = a as FormGroup;
      const fgb = b as FormGroup;

      return fga.value.order.toString().localeCompare(fgb.value.order.toString());
    })
  }
}
