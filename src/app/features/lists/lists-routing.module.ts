import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListFormComponent } from './presentation/components/list-form/list-form.component';
import { ListsListComponent } from './presentation/components/lists-list/lists-list.component';

const routes: Routes = [
  {
    path: '',
    component: ListsListComponent,
    children: [
      {
        path: 'new',
        component: ListFormComponent,
      },
      {
        path: ':id',
        component: ListFormComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListsRoutingModule { }
