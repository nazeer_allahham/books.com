import { Injectable } from '@angular/core';
import { delay, Observable, of } from 'rxjs';
import { JObject } from 'src/app/shared/models/object';
import { Book } from '../../domain/dtos';
import { BookMapper } from '../mappers/book';

@Injectable({
  providedIn: 'root'
})
export class BooksRepository {

  private _data: JObject[];

  constructor() {
    this._data = [
      {
        title: 'Dune',
        year: 1965,
        author: 'Frank Herbert',
      },
      {
        title: 'Ender\'s Game',
        year: 1985,
        author: 'Orson Scott Card',
      },
      {
        title: 1984,
        year: 1949,
        author: 'George Orwell',
      },
      {
        title: 'Fahrenheit 451',
        year: 1953,
        author: 'Ray Bradbury',
      },
      {
        title: 'Brave New World',
        year: 1932,
        author: 'Aldous Huxley',
      },
    ]
  }

  /**
   * Search the available books list.
   * @param q search query
   * @returns {Observable<Book[]>}
   */
  public search(): Observable<Book[]> {
    return of(this._data.map(e => BookMapper.fromJson(e))).pipe(delay(1500));
  }
}
