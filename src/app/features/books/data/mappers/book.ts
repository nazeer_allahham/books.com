import { JObject } from "src/app/shared/models/object";
import { Book } from "../../domain/dtos";

export const BookMapper = {

    /**
     * fromJson
     * @param json
     * @returns {Book}
     */
    fromJson: (json: JObject): Book => {
        return {
            title: json['title'] + '',
            year: Number.parseInt(json['year'] + ''),
            author: json['author'] + '',
        }
    },

    /**
     *
     */
    toJson: (book: Book): JObject => {
        return { title: book.title, year: book.year, author: book.author };
    },
}
