import { Component, Input } from '@angular/core';
import { Book } from '../../../domain/dtos';

@Component({
  selector: 'app-books-grid-item',
  templateUrl: './books-grid-item.component.html',
  styleUrls: ['./books-grid-item.component.scss']
})
export class BooksGridItemComponent {


  @Input()
  public book?: Book;

  constructor() {
  }
}
