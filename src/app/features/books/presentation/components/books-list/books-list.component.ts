import { Component, OnInit } from '@angular/core';
import { catchError } from 'rxjs';
import { Book } from '../../../domain/dtos';
import { BooksService } from '../../../domain/services/books.service';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})
export class BooksListComponent implements OnInit {

  public loading: boolean;
  public books: Book[];
  public error?: Error;

  constructor(private booksService: BooksService) {

    this.loading = true;
    this.books = [];
  }

  public ngOnInit(): void {
    this.onSearch();
  }

  /**
   * On search
   * @param q search query
   */
  public onSearch(): void {
    this.loading = true;
    this.booksService
      .search()
      .pipe(catchError((err, caught) => {
        this.error = err as Error;
        this.loading = false;
        return caught;
      }))
      .subscribe(res => {
        this.books = res;
        this.loading = false;
      });
  }

}
