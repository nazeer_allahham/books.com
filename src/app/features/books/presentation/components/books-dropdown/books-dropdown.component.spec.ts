import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DropdownModule } from 'primeng/dropdown';

import { BooksDropdownComponent } from './books-dropdown.component';

describe('BooksDropdownComponent', () => {
  let component: BooksDropdownComponent;
  let fixture: ComponentFixture<BooksDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BooksDropdownComponent ],
      imports: [
        DropdownModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
