/**
 * Book
 */
export interface Book {
    /**
     * book title
     */
    title: string;

    /**
     * book publish year
     */
    year: number;

    /**
     * book author
     */
    author: string;
}