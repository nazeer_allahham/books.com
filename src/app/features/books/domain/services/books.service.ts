import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BooksRepository } from '../../data/repositories/books.repository';
import { Book } from '../dtos';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private repo: BooksRepository) {
  }

  /**
   * Search the available books list.
   * @param q search query
   * @returns {Observable<Book[]>}
   */
  public search(): Observable<Book[]> {
    return this.repo.search();
  }

}
