import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataViewModule } from 'primeng/dataview';

import { BooksRoutingModule } from './books-routing.module';
import { BooksListComponent } from './presentation/components/books-list/books-list.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { BooksGridItemComponent } from './presentation/components/books-grid-item/books-grid-item.component';
import { BooksDropdownComponent } from './presentation/components/books-dropdown/books-dropdown.component';
import { DropdownModule } from 'primeng/dropdown';
import { SkeletonModule } from 'primeng/skeleton';

@NgModule({
  declarations: [
    BooksListComponent,
    BooksGridItemComponent,
    BooksDropdownComponent
  ],
  imports: [
    CommonModule,
    BooksRoutingModule,
    DataViewModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    DropdownModule,
    SkeletonModule
  ],
  providers: [
  ],
  exports: [
    BooksDropdownComponent
  ]
})
export class BooksModule {

}
