import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';
import { GuestGuard } from './shared/guards/guest.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./features/auth/auth.module').then(e => e.AuthModule),
    canLoad: [GuestGuard],
  },
  {
    path: 'books',
    loadChildren: () => import('./features/books/books.module').then(e => e.BooksModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'lists',
    loadChildren: () => import('./features/lists/lists.module').then(e => e.ListsModule),
    canLoad: [AuthGuard],
  },
  {
    path: '',
    redirectTo: '/books',
    pathMatch: 'full',
  }
  // {
  //   path: '**',
  //   redirectTo: '/books',
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
